PyDays Vienna 2018 Jupyter Workshop
===================================

Installation
------------

Attendees should already install Jupyter Notebook and the required Python packages.

**Option 1: virtualenv**
    
    cd jupyter-workshop-pyvie2018
    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt
    jupyter notebook

**Option 2: Docker**

If you know Docker you can use the existing Jupyter SciPy image where everything is already installed. See the provided `start-notebook.sh`. Note that the docker image size is approx 4GB.


Data
----

In the workshop we will work with the data provided in the `data` directory.

